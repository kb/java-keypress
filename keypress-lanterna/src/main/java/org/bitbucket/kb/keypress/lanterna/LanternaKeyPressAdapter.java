package org.bitbucket.kb.keypress.lanterna;

import java.util.Map;

import org.bitbucket.kb.keypress.KeyModifier;
import org.bitbucket.kb.keypress.KeyPress;
import org.bitbucket.kb.keypress.KeyPress.Builder;
import org.bitbucket.kb.keypress.KeyPressAdapter;
import org.bitbucket.kb.keypress.NonCharacterKey;

import com.google.common.collect.ImmutableMap;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;


public class LanternaKeyPressAdapter implements KeyPressAdapter<KeyStroke>{

	private static final Map<NonCharacterKey,KeyType> nonCharacterToKeyType;
	private static final Map<KeyType,NonCharacterKey> keyTypeToNonCharacter;
	static {
		ImmutableMap.Builder<NonCharacterKey, KeyType> toMap = ImmutableMap.builder();
		ImmutableMap.Builder<KeyType, NonCharacterKey> fromMap = ImmutableMap.builder();
		for (NonCharacterKey nc : NonCharacterKey.values()) {
			// TODO space pause
			switch (nc) {
				case Backspace: case Delete: case End: case Enter: case Escape: case F1:
				case F10: case F11: case F12: case F2: case F3: case F4: case F5: case F6:
				case F7: case F8: case F9: case Tab: case Home:
				case PageDown: case PageUp: case Insert:
					toMap.put(nc, KeyType.valueOf(nc.name()));
					fromMap.put(KeyType.valueOf(nc.name()), nc);
					break;
				case Down: case Left: case Right: case Up:
					toMap.put(nc, KeyType.valueOf("Arrow" + nc.name()));
					fromMap.put(KeyType.valueOf("Arrow" + nc.name()), nc);
					break;
			}
		}
		nonCharacterToKeyType = toMap.build();
		keyTypeToNonCharacter = fromMap.build();
	}

	@Override
	public KeyStroke fromKeyPress(KeyPress kp)
	{
		// TODO Pause
		boolean alt = kp.getModifiers().contains(KeyModifier.ALT);
		boolean ctrl = kp.getModifiers().contains(KeyModifier.CTRL);
		if (kp.isCharacter()) {
			return new KeyStroke(kp.getCharacter(), ctrl, alt);
		} else {
			return new KeyStroke(nonCharacterToKeyType.get(kp.getNonCharacter()), ctrl, alt);
		}
	}

	@Override
	public KeyPress toKeyPress(KeyStroke keyStroke)
	{
		Builder b = new KeyPress.Builder();
		switch(keyStroke.getKeyType()) {
			case Character:
				b.character(keyStroke.getCharacter());
				break;
			case ArrowDown: case ArrowLeft: case ArrowRight: case ArrowUp:
				b.nonCharacter(NonCharacterKey.valueOf(keyStroke.getKeyType().name().replace("Arrow", "")));
				break;
			case Backspace: case Delete: case End: case Enter: case Escape: case F1:
			case F10: case F11: case F12: case F13: case F14: case F15: case F16:
			case F17: case F18: case F19: case F2: case F3: case F4: case F5:
			case F6: case F7: case F8: case F9: case Home: case Insert: case PageDown:
			case PageUp: case Tab:
				b.nonCharacter(NonCharacterKey.valueOf(keyStroke.getKeyType().name()));
				break;
			case Unknown:
			case CursorLocation:
			case EOF:
			case ReverseTab:
				throw new IllegalArgumentException();
		}
		if (keyStroke.isAltDown())
			b.modifier(KeyModifier.ALT);
		if (keyStroke.isCtrlDown())
			b.modifier(KeyModifier.CTRL);
		return b.build();
	}

	@Override
	public Class<KeyStroke> getType()
	{
		return KeyStroke.class;
	}

}
