package org.bitbucket.kb.keypress.lanterna;

import org.bitbucket.kb.keypress.KeyModifier;
import org.bitbucket.kb.keypress.KeyPress;
import org.bitbucket.kb.keypress.NonCharacterKey;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.lanterna.input.KeyStroke;


public class LanternaKeyPressAdapterTest {
	private static final Logger log = LoggerFactory.getLogger(LanternaKeyPressAdapterTest.class);

	@Test
	public void testGetType() throws Exception
	{
		log.debug("{}", new KeyPress.Builder().character('a').modifier(KeyModifier.CTRL).build().toNative(KeyStroke.class));
		log.debug("{}", new KeyPress.Builder().nonCharacter(NonCharacterKey.Down).modifier(KeyModifier.CTRL).build().toNative(KeyStroke.class));
	}

}
