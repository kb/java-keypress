package org.bitbucket.kb.keypress;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public enum NonCharacterKey {
	F1("F1"),
	F2("F2"),
	F3("F3"),
	F4("F4"),
	F5("F5"),
	F6("F6"),
	F7("F7"),
	F8("F8"),
	F9("F9"),
	F10("F10"),
	F11("F11"),
	F12("F12"),
	Tab("Tab"),
	Backspace("BS"),
	Insert("Insert"),
	Delete("Del"),
	Space("Space"),
	Enter("CR"),
	Pause("Pause"),
	Escape("Esc"),
	Home("Home"),
	End("End"),
	PageUp("PageUp"),
	PageDown("PageDown"),
	Left("Left"),
	Right("Right"),
	Up("Up"),
	Down("Down"), ;
	public final String vim;

	private static final Map<String, NonCharacterKey> fromVim;
	static
	{
		Builder<String, NonCharacterKey> map = ImmutableMap.builder();
		for (NonCharacterKey nc : values())
		{
			map.put(nc.vim.toLowerCase(), nc);
		}
		fromVim = map.build();
	}

	public static NonCharacterKey fromVim(String k)
	{
		if (! fromVim.containsKey(k.toLowerCase())) {
			throw new IllegalArgumentException("No such NonCharacterKey: " + k);
		}
		return fromVim.get(k.toLowerCase());
	}

	private NonCharacterKey(String vim)
	{
		this.vim = vim;
	}

}