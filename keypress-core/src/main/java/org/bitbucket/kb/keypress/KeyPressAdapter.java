package org.bitbucket.kb.keypress;

public interface KeyPressAdapter<E> {
	
	Class<E> getType();
	
	E fromKeyPress(KeyPress keyPress);
	
	KeyPress toKeyPress(E nativeKeyPress);
	
	

}
