package org.bitbucket.kb.keypress;

import java.lang.reflect.Modifier;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.reflections.Reflections;

public class KeyPress {

	@SuppressWarnings({ "rawtypes" })
	public static final Map<Class, KeyPressAdapter> adapters = new HashMap<>();

	static
	{
		findAdaptersInClassPath();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void findAdaptersInClassPath()
	{
		Reflections reflections = new Reflections("org.bitbucket.kb.keypress");
		Set<Class<? extends KeyPressAdapter>> subTypesOf = reflections.getSubTypesOf(KeyPressAdapter.class);
		for (Class<? extends KeyPressAdapter> adapterClass : subTypesOf)
		{
			KeyPressAdapter adapter;
			try
			{
				if (Modifier.INTERFACE != adapterClass.getModifiers())
				{
					adapter = adapterClass.newInstance();
					registerAdapter(adapter.getType(), adapter);
				}
			} catch (InstantiationException | IllegalAccessException e)
			{
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> KeyPress fromNative(Class<T> clazz, T nativeKeyPress)
	{
		if (!adapters.containsKey(clazz))
		{
			throw new IllegalArgumentException();
		}
		return adapters.get(clazz).toKeyPress(nativeKeyPress);
	}

	public static KeyPress fromVimString(String vimString)
	{
		if (null == vimString || vimString.length() == 0)
		{
			throw new IllegalArgumentException("No key given.");
		}
		Builder builder = new Builder();
		if (vimString.length() == 1)
		{
			return builder.character(vimString).build();
		}
		if (vimString.length() < 4)
		{
			throw new IllegalArgumentException("Invalid vim key string: " + vimString);
		}
		String inner = vimString.substring(1, vimString.length() - 1);
		String[] tokens = inner.split("-");
		String charOrNonChar = tokens[tokens.length - 1];
		if (charOrNonChar.length() == 1)
		{
			builder.character(charOrNonChar);
		}
		else
		{
			builder.nonCharacter(NonCharacterKey.fromVim(charOrNonChar));
		}
		for (int i = 0; i < tokens.length - 1; i++)
		{
			builder.modifier(KeyModifier.fromVim(tokens[i]));
		}
		return builder.build();
	}

	public static final <T, K extends KeyPressAdapter<T>> void registerAdapter(Class<T> clazz, K adapter)
	{
		adapters.put(clazz, adapter);
	}

	private final Character character;

	private final EnumSet<KeyModifier> modifiers;

	private final NonCharacterKey nonCharacter;

	private KeyPress(char character, EnumSet<KeyModifier> modifiers)
	{
		this.character = character;
		this.modifiers = modifiers;
		this.nonCharacter = null;
	}

	private KeyPress(NonCharacterKey nonCharacter, EnumSet<KeyModifier> modifiers)
	{
		this.character = null;
		this.modifiers = modifiers;
		this.nonCharacter = nonCharacter;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KeyPress other = (KeyPress) obj;
		if (getCharacter() == null)
		{
			if (other.getCharacter() != null)
				return false;
		}
		else if (!getCharacter().equals(other.getCharacter()))
			return false;
		if (getModifiers() == null)
		{
			if (other.getModifiers() != null)
				return false;
		}
		else if (!getModifiers().equals(other.getModifiers()))
			return false;
		if (getNonCharacter() != other.getNonCharacter())
			return false;
		return true;
	}

	public boolean equalsVimString(String other)
	{
		KeyPress otherKP = KeyPress.fromVimString(other);
		return equals(otherKP);
	}

	public Character getCharacter()
	{
		return character;
	}

	public Character getCharacterNormalized()
	{
		return modifiers.contains(KeyModifier.SHIFT) ? Character.toUpperCase(character) : character;
	}

	public EnumSet<KeyModifier> getModifiers()
	{
		return modifiers;
	}

	public NonCharacterKey getNonCharacter()
	{
		return nonCharacter;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getCharacter() == null) ? 0 : getCharacter().hashCode());
		result = prime * result + ((getModifiers() == null) ? 0 : getModifiers().hashCode());
		result = prime * result + ((getNonCharacter() == null) ? 0 : getNonCharacter().hashCode());
		return result;
	}

	public boolean isCharacter()
	{
		return getCharacter() != null;
	}

	public boolean isCharacterOnly()
	{
		return isCharacter() && (
				modifiers.isEmpty()
				||
				(modifiers.size() == 1 && modifiers.contains(KeyModifier.SHIFT)));
	}

	@SuppressWarnings("unchecked")
	public <T> T toNative(Class<T> clazz)
	{
		if (!adapters.containsKey(clazz))
		{
			throw new IllegalArgumentException();
		}
		return (T) adapters.get(clazz).fromKeyPress(this);
	}

	@Override
	public String toString()
	{
		return "KeyPress [character=" + character + ", modifiers=" + modifiers + ", nonCharacter=" + nonCharacter + "]";
	}

	public String toVimString()
	{
		StringBuilder sb = new StringBuilder();
		// Special case: Uppercase letter
		if (getModifiers() != null && getModifiers().size() == 1 && getModifiers().contains(KeyModifier.SHIFT))
		{
			sb.append(Character.toUpperCase(this.getCharacter()));
			return sb.toString();
		}
		if (getModifiers() != null)
		{
			for (KeyModifier mod : getModifiers())
			{
				sb.append(mod.vim);
				sb.append("-");
			}
		}
		if (getCharacter() != null)
		{
			sb.append(getCharacter());
		}
		else if (getNonCharacter() != null)
		{
			sb.append(getNonCharacter().vim);
		}
		if (getNonCharacter() != null || (getModifiers() != null && !getModifiers().isEmpty()))
		{
			sb.insert(0, "<");
			sb.append(">");
		}
		return sb.toString();
	}

	public static class Builder {
		private Character character = null;
		private EnumSet<KeyModifier> modifiers = EnumSet.noneOf(KeyModifier.class);
		private NonCharacterKey nonCharacter = null;

		public KeyPress build()
		{
			if (null != character)
			{
				return new KeyPress(character, modifiers);
			}
			else
			{
				return new KeyPress(nonCharacter, modifiers);
			}
		}

		public Builder character(char charAt)
		{
			if (Character.isUpperCase(charAt))
			{
				modifier(KeyModifier.SHIFT);
				this.character = Character.toLowerCase(charAt);
			}
			else
			{
				this.character = charAt;
			}
			this.nonCharacter = null;
			return this;
		}

		public Builder character(String character)
		{
			return character(character.charAt(0));
		}

		public Builder modifier(KeyModifier mod)
		{
			this.modifiers.add(mod);
			return this;
		}

		public Builder nonCharacter(NonCharacterKey nonChar)
		{
			this.character = null;
			this.nonCharacter = nonChar;
			return this;
		}
	}
}